package com.study.stringcalculator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jason.shim on 2016. 11. 22..
 */
public class StringCalculator {

    public int add(String text) {
        if (isEmpty(text))
            return 0;

        String[] params = getParams(text);
        return getSum(stringToInt(params));
    }

    public int getSum(int[] params) {
        int sum = 0;
        for (int param : params) {
            sum += param;
        }
        return sum;
    }

    public int[] stringToInt(String[] params) {
        int[] intArray = new int[params.length];
        for (int i = 0; i < params.length; i++)
            intArray[i] = toPositive(params[i]);
        return intArray;
    }

    public int toPositive(String param) {
        int value = Integer.parseInt(param);
        if (value < 0)
            throw new RuntimeException();
        return value;
    }

    public String[] getParams(String text) {
        Matcher m = Pattern.compile("//(.)\n(.*)").matcher(text);
        if (m.find())
            return m.group(2).split(m.group(1));
        return text.split(",|:");
    }

    public boolean isEmpty(String text) {
        return text == null || text.isEmpty();
    }
}
