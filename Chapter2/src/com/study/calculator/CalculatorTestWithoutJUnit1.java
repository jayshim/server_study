package com.study.calculator;

/**
 * Created by jason.shim on 2016. 11. 22..
 */
public class CalculatorTestWithoutJUnit1 {
    public static void main(String[] args) {
        Calculator cal = new Calculator();
        System.out.println(cal.add(9, 3));
        System.out.println(cal.substract(9, 3));
        System.out.println(cal.multiply(9, 3));
        System.out.println(cal.divide(9, 3));
    }
}
