package com.study.stringcalculator;

import com.study.stringcalculator.StringCalculator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by jason.shim on 2016. 11. 22..
 */
public class StringCalculatorTest {
    StringCalculator stringCalculator;

    @Before
    public void setUp() throws Exception {
        stringCalculator = new StringCalculator();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void add() throws Exception {
        assertEquals(0, stringCalculator.add(""));
        assertEquals(0, stringCalculator.add(null));
    }

    @Test
    public void addOne() throws Exception {
        assertEquals(1, stringCalculator.add("1"));
    }

    @Test
    public void addMultipleParams() throws Exception {
        assertEquals(6, stringCalculator.add("1,2,3"));
    }


    @Test
    public void isEmpty() throws Exception {
        assertEquals(true, stringCalculator.isEmpty(""));
        assertEquals(true, stringCalculator.isEmpty(null));
    }

    @Test
    public void getParamsByComma() throws Exception {
        assertArrayEquals(new String[] {"1", "2", "3"}, stringCalculator.getParams("1,2,3"));
    }

    @Test
    public void getParamsByColon() throws Exception {
        assertArrayEquals(new String[] {"1", "2", "3"}, stringCalculator.getParams("1:2:3"));
    }

    @Test
    public void getParamsByNonNumericChars() throws Exception {
        assertArrayEquals(new String[] {"1", "2", "3"}, stringCalculator.getParams("//;\n1;2;3"));
    }

    @Test
    public void getSum() throws Exception {
        assertEquals(6, stringCalculator.getSum(new int[] {1, 2, 3}));
    }

    @Test
    public void stringToInt() throws Exception {
        assertArrayEquals(new int[] {1, 2, 3}, stringCalculator.stringToInt(new String[] {"1", "2", "3"}));
    }

    @Test(expected = RuntimeException.class)
    public void addNegative() throws Exception {
        stringCalculator.add("-1,2,3");
    }


}