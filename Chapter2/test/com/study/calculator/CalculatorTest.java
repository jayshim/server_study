package com.study.calculator;

import com.study.calculator.Calculator;

import static org.junit.Assert.*;

/**
 * Created by jason.shim on 2016. 11. 22..
 */
public class CalculatorTest {
    @org.junit.Test
    public void add() throws Exception {
        Calculator cal = new Calculator();
        assertEquals(9, cal.add(6, 3));
    }

    @org.junit.Test
    public void substract() throws Exception {
        Calculator cal = new Calculator();
        assertEquals(3, cal.substract(6, 3));
    }

    @org.junit.Test
    public void multiply() throws Exception {
        Calculator cal = new Calculator();
        assertEquals(18, cal.multiply(6, 3));
    }

    @org.junit.Test
    public void divide() throws Exception {
        Calculator cal = new Calculator();
        assertEquals(2, cal.divide(6, 3));
    }

}