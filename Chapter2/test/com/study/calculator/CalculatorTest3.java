package com.study.calculator;

import com.study.calculator.Calculator;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by jason.shim on 2016. 11. 22..
 */
public class CalculatorTest3 {
    Calculator cal;

    @Before
    public void setUp() throws Exception {
        cal = new Calculator();
    }

    @Test
    public void add() throws Exception {
        assertEquals(9, cal.add(6, 3));
    }

    @Test
    public void substract() throws Exception {
        assertEquals(3, cal.substract(6, 3));
    }

    @Test
    public void multiply() throws Exception {
        assertEquals(18, cal.multiply(6, 3));
    }

    @Test
    public void divide() throws Exception {
        assertEquals(2, cal.divide(6, 3));
    }

}