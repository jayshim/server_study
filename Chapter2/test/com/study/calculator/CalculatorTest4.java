package com.study.calculator;

import com.study.calculator.Calculator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by jason.shim on 2016. 11. 22..
 */
public class CalculatorTest4 {
    Calculator cal;

    @Before
    public void setUp() throws Exception {
        cal = new Calculator();
        System.out.println("before");
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("teardown");
    }

    @Test
    public void add() throws Exception {
        assertEquals(9, cal.add(6, 3));
        System.out.println("add");
    }

    @Test
    public void substract() throws Exception {
        assertEquals(3, cal.substract(6, 3));
        System.out.println("substract");
    }

    @Test
    public void multiply() throws Exception {
        assertEquals(18, cal.multiply(6, 3));
        System.out.println("multiply");
    }

    @Test
    public void divide() throws Exception {
        assertEquals(2, cal.divide(6, 3));
        System.out.println("divide");
    }

}